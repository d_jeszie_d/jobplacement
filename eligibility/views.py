from django.shortcuts import render
from django.views.generic import DetailView
from django.views.generic.edit import CreateView, UpdateView

from .forms import EligibleForm
from .models import Eligible

def Index(request, s):
    print("request")
    print(request)
    s1 = request.GET.get('s1', '')
    print(s1)
    print(f"the value of s is: {s}")
    return render(
            request, template_name='index.html',
            context={'s': s}
    )

def Eligibles(request):
    type_of_eligibility = request.GET.get('type_of_eligibility', '')
    query = Eligible.objects.all()
    if type_of_eligibility:
        query = Eligible.objects.filter(type__name=type_of_eligibility)
    return render(
        request, template_name='eligibles.html',
        context={'eligibles': query}
    )

class Add_Eligibility(CreateView):
    model = Eligible
    form_class = EligibleForm
    template_name = 'add_eligibility.html'
    success_url = '/'

class Detail_Elibility(DetailView):
    model = Eligible
    template_name = 'detail_eligibility.html'

class Edit_Eligibility(UpdateView):
    model = Eligible
    form_class = EligibleForm
    template_name = 'edit_eligibility.html'
    success_url = '/'