from django.db.models import (
  DO_NOTHING, CharField, DateField, DateTimeField, ForeignKey,
  Model, TextField, ImageField, DecimalField, FileField,
)

class Type_of_Eligibility(Model):
    name = CharField(max_length=128)
    description = TextField()

    def __str__(self):
        # return f"{self.id}, {self.name}"
        return f"{self.name}"

class Eligible(Model):
    name = CharField(max_length=128)
    type_of_eligibility = ForeignKey(Type_of_Eligibility, on_delete=DO_NOTHING)
    rating = DecimalField(max_digits=4, decimal_places=2)
    place_of_exam = CharField(max_length=128)
    date_of_exam = DateField()
    created = DateTimeField(auto_now_add=True)
    image = ImageField(upload_to='images/')
    video = FileField(upload_to='videos/', null=True, blank="True")

    def __str__(self):
        # return f"{self.id}, {self.name}"
        return f"{self.name}, {self.type_of_eligibility}"