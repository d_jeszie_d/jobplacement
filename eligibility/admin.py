from django.contrib import admin
from . models import Type_of_Eligibility
from . models import Eligible

# Register your models here.
admin.site.register(Type_of_Eligibility),
admin.site.register(Eligible)