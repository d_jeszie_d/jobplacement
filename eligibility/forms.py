from django import forms

from .models import Eligible


class EligibleForm(forms.ModelForm):
    class Meta:
        model = Eligible
        fields = (
            'name', 'type_of_eligibility', 'rating',
            'place_of_exam', 'date_of_exam', 'image',
        )
