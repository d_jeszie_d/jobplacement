from django.urls import path

from accounts.views import (
		account_view,
		edit_account_view,
		crop_image,
)

app_name = 'account'

urlpatterns = [
	path('<user_id>/', account_view, name="view"),
	path('<user_id>/edit/', edit_account_view, name="edit"),
	path('<user_id>/edit/cropImage/', crop_image, name="crop_image"),
]

# from django.urls import path, include
# from . import views
# urlpatterns = [
#     path('signup/', views.register_view, name='signup'),
#     path('login/', views.login_view, name='login'),
#     path('logout/', views.logout_view, name='logout'),
# ]
